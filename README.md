#SINEVIA PLUGIN SETTINGS

A plugin that allows project setiings to be saved to an SQL table


# Installation #

```
#!json

   "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/phplibrary/plugin-settings.git"
        }
    ],
    "require": {
        "php": ">=5.5.9",
        "sinevia/phplibrary/plugin-settings": "dev-master"
    },
```

# How to Use? #