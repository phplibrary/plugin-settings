<?php

namespace Sinevia\Plugins;

class SettingsPlugin {

    /**
     * @var \Sinevia\SqlDb
     */
    protected static $db = null;

    /**
     * @var string
     */
    protected static $table = null;

    public static function configure($options) {
        $pdo = isset($options['pdo']) ? $options['pdo'] : null;
        self::$table = isset($options['table']) ? $options['table'] : 'snv_plugin_settings';
        if ($pdo == null) {
            throw new \RuntimeException('Required option "pdo" is missing');
        }
        self::$db = new \Sinevia\SqlDb();
        self::$db->setPdo($pdo);
        if (self::$db->table(self::$table)->exists() == false) {
            self::install();
        }
    }

    public static function install() {
        if (self::$db->table(self::$table)->exists() == true) {
            return true;
        }
        self::$db->table(self::$table)
                ->column('Id', 'INTEGER')
                ->column('Key', 'STRING')
                ->column('Value', 'TEXT')
                ->column('CreatedAt', 'DATETIME')
                ->column('UpdatedAt', 'DATETIME')
                ->create();
    }

    public static function set($key, $value) {
        $exists = self::$db->table(self::$table)
                        ->where('Key', '=', $key)
                        ->selectOne() == null ? true : false;
        if ($exists == false) {
            self::$db->table(self::$table)->insert(array(
                'Id' => time(),
                'Key' => $key,
                'Value' => json_encode($value),
                'CreatedAt' => date('Y-m-d H:i:s'),
                'UpdatedAt' => date('Y-m-d H:i:s'),
            ));
        }
        if ($exists == true) {
            self::$db->table(self::$table)
                    ->where('Key', '=', $key)
                    ->update(array(
                        'Value' => json_encode($value),
                        'UpdatedAt' => date('Y-m-d H:i:s'),
            ));
        }
    }

    public static function get($key, $default = null) {
        $row = self::$db->table(self::$table)
                ->where('Key', '=', $key)
                ->selectOne();
        if ($row == null) {
            return $default;
        }
        $value = $row['Value'];
        return $value;
    }

}
